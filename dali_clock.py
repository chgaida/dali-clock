#!/usr/bin/env python

"""
A simple digital clock.
"""

import time
import threading
import curses
import digits


DIGIT = digits.DIGIT_M
DIGIT_WIDTH = len(DIGIT["0"][0])
DIGIT_HEIGHT = len(DIGIT["0"])

# Clock position
Y_ORIGIN = 3
X_ORIGIN = 0


def print_digit(win, y, x, digit, delay=0):
    """Print single digit."""
    for row_y, line in enumerate(digit, y):
        row = [(" ", "░")[c == True] for c in line]
        win.addstr(row_y, x, "".join(row), curses.color_pair(1))
        time.sleep(delay)
        win.refresh()


def print_time(win, delay=0):
    """Print the time."""
    current_time = time.strftime("%H:%M", time.localtime())
    x = X_ORIGIN
    for digit in list(current_time):
        print_digit(win, Y_ORIGIN, x, DIGIT[digit], delay)
        x += DIGIT_WIDTH


def clock(win):
    """Main thread to update the clock."""
    while 1:
        # sync time
        while time.strftime("%S", time.localtime()) != "00":
            time.sleep(0.5)
        print_time(win, 0.03)


def tick(win):
    """The blinking colon."""
    colon_visible = True
    x_colon = X_ORIGIN + (2 * DIGIT_WIDTH)
    while 1:
        if colon_visible:
            print_digit(win, Y_ORIGIN, x_colon, DIGIT[":"])
        else:
            print_digit(win, Y_ORIGIN, x_colon, DIGIT[" "])

        colon_visible = not colon_visible
        time.sleep(1)


def init_curses():
    """Init curses."""
    global X_ORIGIN
    # Init
    screen = curses.initscr()
    curses.noecho()
    curses.cbreak()
    curses.curs_set(0)
    screen.keypad(1)

    # Colors
    curses.start_color()
    curses.use_default_colors()
    curses.init_pair(1, 5, -1)

    # Window and background color
    screen.refresh()

    _, width = screen.getmaxyx()
    X_ORIGIN = int((width - (5 * DIGIT_WIDTH)) / 2)

    win = curses.newwin(12, width, 0, 0)
    win.bkgd(curses.color_pair(1))
    win.box()

    return screen, win


def main():
    """Start"""
    try:
        screen, win = init_curses()

        # win.addstr(1, X_ORIGIN, "Dali Clock", curses.color_pair(3)|curses.A_BOLD)

        print_time(win)

        t_tick = threading.Thread(target=tick, args=(win,))
        t_tick.daemon = True
        t_tick.start()
        t_clock = threading.Thread(target=clock, args=(win,))
        t_clock.daemon = True
        t_clock.start()

        # Wait for keypress ('q') to quit
        char = 0
        while char != 113:
            char = screen.getch()

        screen.keypad(0)

    finally:
        # Finish
        curses.nocbreak()
        curses.echo()
        curses.endwin()


if __name__ == "__main__":
    main()
